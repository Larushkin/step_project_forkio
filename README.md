<h1>Hello!</h1>
<h2 align="center">Developers: <a href="https://gitlab.com/Larushkin" target="_blank"> <b>Vladislav Larushkin</b></a>, <br> <a href="" target="_blank"> <b>Diana Ponomarenko</b></a></h2>
<h3 align="center">In our project we used: </h3>
<p>-gulp</p>
<p>-gulp-sass</p>
<p>-browser-sync</p>
<p>-gulp-js-minify</p>
<p>-gulp-uglify</p>
<p>-gulp-clean-css</p>
<p>-gulp-clean</p>
<p>-gulp-concat</p>
<p>-gulp-imagemin</p>
<p>-gulp-autoprefixer</p>
<h3>If you want open our project, please, enter:<br> <b>step 1</b> - "npm init", <br> <b>step 2</b> - "gulp build", <br> <b>step 3</b> -"gulp dev"!</h3>
<h4>Vladislav worked with a header section (burger menu) and section "People Are Talking About Fork".<br>
Diana created main section(Revolutionary Editor, Here is what you get) and footer(Fork Subscription Pricing).</h4>
